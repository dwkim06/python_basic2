
import requests
from bs4 import BeautifulSoup
import sqlite3

def get_item_list():
    url = "https://www.wconcept.co.kr/Men/001001"

    response = requests.get(url)
    html = response.text
    
    soup = BeautifulSoup(html, "html.parser")

    article_info = soup.find_all("div", attrs={"class": "text max"})

    item_list = []

    for article in article_info:
        text_wrap = article.find("div", attrs={"class": "text_wrap"})
        brand_name = text_wrap.find("div", attrs={"class": "brand"}).get_text()
        product_name = text_wrap.find("div", attrs={"class": "product ellipsis multiline"}).get_text()
        price = article.find("div", attrs={"class": "price"}).find("span", attrs={"class": "discount_price"}).get_text()
        
        item = [brand_name, product_name, price.replace(',','')]
        item_list.append(item)

    return item_list

def main():
    item_list = get_item_list()

    # DB 생성 & Auto Commit(Rollback)
    conn = sqlite3.connect("./dwkim_database.db", isolation_level=None)

    # Cursor
    c = conn.cursor()

    # 테이블 생성
    c.execute("CREATE TABLE IF NOT EXISTS items(id INTEGER PRIMARY KEY AUTOINCREMENT, \
        brand_name TEXT, product_name TEXT, price INTEGER)")

    c.executemany("INSERT INTO items(brand_name, product_name, price) \
        VALUES (?,?,?)", item_list)

    conn.close()

if __name__ == "__main__":
    main()